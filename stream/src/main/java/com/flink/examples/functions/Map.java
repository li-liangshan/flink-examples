package com.flink.examples.functions;

import com.flink.examples.DataSource;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.List;

/**
 * @Description Map算子：对数据流一对一的加载计算，并返回一个新的对象
 * @Author JL
 * @Date 2020/09/14
 * @Version V1.0
 */
public class Map {

    /**
     * 遍历集合，打印名称
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        List<Tuple3<String,String,Integer>> tuple3List = DataSource.getTuple3ToList();
        DataStream<String> dataStream = env.fromCollection(tuple3List).map(new MapFunction<Tuple3<String,String,Integer>, String>() {
            @Override
            public String map(Tuple3<String, String, Integer> tuple3s) throws Exception {
                return tuple3s.f0;
            }
        });
        dataStream.print();
        env.execute("flink map job");
    }
}

/*
4> 王五
3> 李四
1> 刘六
3> 吴八
2> 张三
2> 伍七
*/