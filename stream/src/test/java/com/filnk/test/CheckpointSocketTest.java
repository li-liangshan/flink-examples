package com.filnk.test;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * @Description
 * @Author JL
 * @Date 2021/10/27
 * @Version V1.0
 */
public class CheckpointSocketTest {
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 配置Checkpoint
        env.enableCheckpointing(1000);
        //flink支持的各种文件系统参见：https://ci.apache.org/projects/flink/flink-docs-release-1.13/zh/docs/deployment/filesystems/overview/
        //hdfs://是以hodoop做为文件系统存放flink的checkpoint快照数据
        //env.setStateBackend(new FsStateBackend("hdfs://localhost:9000/flink/checkpoint"));
        //file:///是以主机本地文件目录存放flink的checkpoint快照数据(在该目录下会写入窗口数据，当发生错误job退出再次重后，会从最新快照恢复缓存的数据流重新丢入算子中计算)
        env.setStateBackend(new FsStateBackend("file:///test/checkpoint/"));
        //检查点之间间隔500ms，再触发创建下一个检查点；
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        //1分钟内完成检查点快照生成，否则抛弃
        env.getCheckpointConfig().setCheckpointTimeout(60000);
        //被cancel后，保留Checkpoint数据，以便根据实际需要恢复到指定的Checkpoint
        //ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION
        //被cancel后，删除Checkpoint数据，只有job执行失败的时候才会保存Checkpoint
        //CheckpointConfig.ExternalizedCheckpointCleanup.DELETE_ON_CANCELLATION;
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        // 配置失败重启策略：0失败后最多重启3次 每次重启间隔10s
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 10000));
        DataStream<String> source = env.socketTextStream("192.168.110.35", 7001, "\n");
        DataStream<Integer> count = source.flatMap(new FlatMapFunction<String, Integer>() {
            @Override
            public void flatMap(String value, Collector<Integer> out) throws Exception {
                // 失败信号
                if (("ERROR").equals(value)) {
                    String errorMsg = "error content, run is exit!";
                    System.out.println(errorMsg);
                    throw new RuntimeException(errorMsg);
                }
                out.collect(value.length());
            }
        }).returns(Types.INT);
        count.print().setParallelism(1);
        env.execute("Flink Checkpoint Example");
    }
}
