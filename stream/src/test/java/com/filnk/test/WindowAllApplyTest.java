package com.filnk.test;

import org.apache.commons.lang3.RandomUtils;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.math.BigDecimal;
import java.util.Iterator;

/**
 * @Description
 * @Author JL
 * @Date 2021/01/25
 * @Version V1.0
 */
public class WindowAllApplyTest {


    /**
     * 遍历集合，分别打印不同性别的总人数与年龄之和
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //env.setParallelism(1);
        DataStream<Tuple3<Long, BigDecimal, BigDecimal>> inStream = env.addSource(new MyRichSourceFunction());
        DataStream<Tuple2<BigDecimal, BigDecimal>> dataStream = inStream
                //按时间窗口滑动，每6秒为一个时间窗口，并每次滑动1秒（简单来说：每隔1秒对前6秒内的输入数据流），计算一次
                .timeWindowAll(Time.seconds(6), Time.seconds(1))
                .apply(new AllWindowFunction<Tuple3<Long, BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow window, Iterable<Tuple3<Long, BigDecimal, BigDecimal>> input, Collector<Tuple2<BigDecimal, BigDecimal>> out) throws Exception {
                        Iterator<Tuple3<Long, BigDecimal, BigDecimal>> iterator = input.iterator();
                        BigDecimal _in = new BigDecimal(0);
                        BigDecimal _out = new BigDecimal(0);
                        int i = 0;
                        while (iterator.hasNext()){
                            Tuple3<Long, BigDecimal, BigDecimal> tuple3 = iterator.next();
                            _in = _in.add(tuple3.f1);
                            _out = _out.add(tuple3.f2);
                            i ++;
                            System.out.print(tuple3.f1.setScale(2, BigDecimal.ROUND_HALF_UP) +"="+ tuple3.f2.setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
                        }
                        //System.out.println("共:"+i+"个，in sum:"+ _in.setScale(2, BigDecimal.ROUND_HALF_UP) + ",out sum:" + _out.setScale(2, BigDecimal.ROUND_HALF_UP));
                        out.collect(Tuple2.of(_in.setScale(2, BigDecimal.ROUND_HALF_UP) , _out.setScale(2, BigDecimal.ROUND_HALF_UP)));
                    }
                });
        dataStream.print();
        env.execute("flink Filter job");
    }


    /**
     * 模拟数据持续输出
     */
    public static class MyRichSourceFunction extends RichSourceFunction<Tuple3<Long, BigDecimal, BigDecimal>> {
        @Override
        public void run(SourceContext<Tuple3<Long, BigDecimal, BigDecimal>> ctx) throws Exception {
            for (int i=0;i<50;i++){
                ctx.collect(Tuple3.of(System.currentTimeMillis() / 1000, new BigDecimal(RandomUtils.nextDouble(10.00, 50.00)),new BigDecimal(RandomUtils.nextDouble(50.00, 100.99))));
                //1秒钟输出一个
                Thread.sleep(1 * 1000);
            }
        }
        @Override
        public void cancel() {
            try{
                super.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
