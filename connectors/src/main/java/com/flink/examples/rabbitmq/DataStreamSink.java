package com.flink.examples.rabbitmq;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.rabbitmq.RMQSink;
import org.apache.flink.streaming.connectors.rabbitmq.common.RMQConnectionConfig;

/**
 * @Description 将DataStream流中的数据输出到rabbitMq队列中
 * @Author JL
 * @Date 2020/09/18
 * @Version V1.0
 */
public class DataStreamSink {

    /**
     * 官方文档：https://ci.apache.org/projects/flink/flink-docs-release-1.11/zh/dev/connectors/rabbitmq.html
     */

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        RMQConnectionConfig connectionConfig = new RMQConnectionConfig.Builder()
                .setHost("127.0.0.1")
                .setPort(5672)
                .setUserName("admin")
                .setPassword("admin")
                .setVirtualHost("datastream")
                .build();

        String [] words = new String[]{"props","student","build","name","execute"};
        final DataStream<String> stream = env.fromElements(words);
        stream.addSink(new RMQSink<String>(connectionConfig,"test",new SimpleStringSchema()));
        env.execute("flink rabbitMq sink");
    }
}
